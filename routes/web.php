<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@index" );
Route::get('/isidata', "AuthController@index" );
Route::post('/submit', "AuthController@submit" );

Route::get('/master', function(){
    return view('layout.master');
});
Route::get('/table', function(){
    return view('table.table');
});
Route::get('/data-table', function(){
    return view('table.data-table');
});